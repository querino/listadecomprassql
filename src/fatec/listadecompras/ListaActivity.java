package fatec.listadecompras;

//import java.util.ArrayList;

import android.app.ListActivity;
//import android.content.Intent;
import android.os.Bundle;
import android.view.View;
//import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class ListaActivity extends ListActivity {
    /** Called when the activity is first created. */
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        CursorAdapter produtosAdapter = new SimpleCursorAdapter(this, 
        		android.R.layout.simple_list_item_checked, null,
        		new String[] { "nome" }, new int[] { android.R.id.text1 });
        this.setListAdapter(produtosAdapter);
        this.getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        Dao dao = new Dao(this);
        dao.abrir();
        produtosAdapter.changeCursor(dao.obterProdutos());
        dao.fechar();
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
   
    	l.setItemChecked(position, l.getCheckedItemPositions().get(position));
    	
    	Object objeto = this.getListAdapter().getItem(position);
    	String produto = objeto.toString();
    	Toast.makeText(this, "Voce selecionou: " + produto, Toast.LENGTH_SHORT).show();
    }
}