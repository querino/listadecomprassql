package fatec.listadecompras;

//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TelaPrincipalActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.principal);
	}
	
	public void toqueBotaoIncluir(View v) {
		Intent it = new Intent(this, TelaIncluirActivity.class);
		startActivity(it);
	}
	
	public void toqueBotaoExcluir(View v) {
		Intent it = new Intent(this, TelaExcluirActivity.class);
		startActivity(it);
	}
	
	public void toqueLista(View v) {
		Intent it = new Intent(this, ListaActivity.class);
		startActivity(it);
	}
}
