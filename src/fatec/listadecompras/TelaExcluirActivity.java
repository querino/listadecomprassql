package fatec.listadecompras;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.view.View;

public class TelaExcluirActivity extends ListActivity {
	
	private Dao dao;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        CursorAdapter produtosAdapter = new SimpleCursorAdapter(this, 
        		android.R.layout.simple_list_item_2, null,
        		new String[] { "nome" }, new int[] { android.R.id.text1 });
        this.setListAdapter(produtosAdapter);
        this.getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        dao = new Dao(this);
        dao.abrir();
        produtosAdapter.changeCursor(dao.obterProdutos());
        dao.fechar();
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		dao.removerProduto(id);
		finish();
	}
}
