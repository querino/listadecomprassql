package fatec.listadecompras;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class TelaIncluirActivity extends Activity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.incluirproduto);
	}
	
	public void toqueBotao(View v) {
		EditText edtProduto = (EditText)findViewById(R.id.edtProduto);
		Dao dao = new Dao(this);
		dao.inserirProduto(edtProduto.getText().toString());
		finish();
	}
}
